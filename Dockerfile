FROM php:7.3-apache

WORKDIR /var/www/html

RUN apt-get update \
  && apt-get install -y \
    unzip \
    # for intl extension
    libicu-dev \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    curl \
  && docker-php-ext-configure intl \
  && docker-php-ext-install intl pdo pdo_mysql gd mbstring

RUN a2enmod rewrite

# Install vanilla using pre-built version
RUN curl --output vanilla-core-2021-009.zip \
  "https://open.vanillaforums.com/get/vanilla-core-2021.009.zip" \
  && unzip -q vanilla-core-2021-009.zip \
  && rm vanilla-core-2021-009.zip \
  && find package ! -path package -prune -exec mv {} . \; \
  && rmdir package

RUN cp .htaccess.dist .htaccess

# Use custom config file
COPY config/structure.php applications/dashboard/settings/structure.php
COPY config/config.php applications/dashboard/settings/config.php

# Set directories writable
RUN chmod -R 777 conf cache uploads

RUN ls -al
